/** @jsx jsx */
import React from 'react'
import { jsx, css } from '@emotion/core'
import { Card } from 'antd'

const jigsawStyle = css`
  cursor: pointer;
  width: 100%;
  height: 100%;
  .ant-card-body {
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 50px;
  }
`

const Jigsaw = ({ number, display }) => (
  <Card
    css={jigsawStyle}
    style={{ opacity: display ? 1 : 0, }
  }>
    {number}
  </Card>
)

export default Jigsaw
