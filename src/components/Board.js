/** @jsx jsx */
import React from 'react'
import { jsx, css } from '@emotion/core'
import { Col } from 'antd'
import Jigsaw from './Jigsaw'

const boardStyle = css`
  width: 500px;
  height: 500px;
  position: relative;
  img {
    width: 500px;
    height: 500px;
  }
  .img,
  .jigsaw-group {
    position: absolute;
    top: 0;
  }
  .jigsaw {
    width: 20%;
    height: 100px;
  }
`

const imagePath = (categoty, name) => `${process.env.PUBLIC_URL}/assets/images/${categoty}/${name}`

const Board = ({ jigsaws, onClick, category, image }) => {
  return (
    <Col className="quiz-board" css={boardStyle}>
      <Col className="img">
        <img alt="" src={imagePath(category, image)} />
      </Col>
      <Col className="jigsaw-group">
        {jigsaws.map((state, index) => {
          return (
            <Col
              key={index}
              span={5}
              onClick={onClick(index)}
              className="jigsaw"
            >
              <Jigsaw
                number={index+1}
                display={state}
              />
            </Col>
          )
        })}
      </Col>
    </Col>
  )
}

export default Board
