/** @jsx jsx */
import React, { PureComponent } from 'react'
import { jsx, css } from '@emotion/core'
import { shuffle, lowerCase } from 'lodash'
import { Row, Col, Input, notification } from 'antd'
import Board from './Board'

const quizStyle = css`
  padding: 20px;
  .title,
  .answer,
  .end {
    text-align: center;
  }
  .title {
    color: #4285f4;
    font-size: 30px;
    padding-bottom: 30px;
  }
  .info {
    display: grid;
    border-radius: 5px;
    background: #939eab;
    color: white;
    margin-bottom: 30px;
    padding: 5px 0px;
  }
  .stage,
  .score {
    width: 500px;
    font-size: 16px;
    padding-left: 10px;
  }
  .board {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .ant-input {
    width: 500px;
    height: 50px;
  }
  .end.finished {
    font-size: 30px;
  }
  .end.description {
    font-size: 20px;
  }
  .end.total-score {
    font-size: 50px;
  }
  .end-info {
    border: 1px solid black;
    border-radius: 5px;
    padding: 15px;
  }
`

class Quiz extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      category: 'sport',
      stages: [
        { image:'sport01.jpg', answer: 'baseball' },
        { image:'sport02.jpg', answer: 'football' },
        { image:'sport03.jpg', answer: 'golf' },
        { image:'sport04.jpg', answer: 'bowling' },
        { image:'sport05.jpg', answer: 'fencer' },
      ],
      stage: 0,
      jigsaws: [
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
      ],
      maxScore: 25,
      score: 0,
    }
  }

  componentDidMount() {
    const { stages } = this.state
    const newStages = shuffle(stages)
    this.setState({ stages: newStages })
  }

  openNotification = (type, score) => {
    switch (type) {
      case 'correct':
        notification['success']({
          message: 'Correct!',
          description: `Your score is increased by ${score}.\nGoing to next stage...`,
          duration: 5,
        })
        break
      case 'incorrect':
        notification['error']({
          message: 'Incorrect!',
          description: `Now, your max score is ${score}.`,
          duration: 5,
        })
        break
      default:
        break
    }
  }

  onClickJigsaw = (index) => () => {
    const { jigsaws } = this.state
    const newJigsaws = jigsaws.map((state, pos) => {
      if (pos === index) return false
      return state
    })
    this.setState({ jigsaws: newJigsaws }) 
  }

  onEnterAnswer = ({ target: { value } }) => {
    const { stages, stage, jigsaws, score, maxScore } = this.state
    const { answer } = stages[stage]
    if (answer === lowerCase(value)) {
      const newStage = stage + 1
      let scoreForAdd = jigsaws.filter((state) => state === true).length
      if (scoreForAdd > maxScore) scoreForAdd = maxScore
      const newScore = score + scoreForAdd
      const newJigsaws = jigsaws.map(() => true)
      this.setState({
        stage: newStage,
        maxScore: 25,
        score: newScore,
        jigsaws: newJigsaws,
      })
      this.openNotification('correct', scoreForAdd)
    } else {
      const newMaxScore = maxScore - 1
      this.setState({ maxScore: newMaxScore })
      this.openNotification('incorrect', newMaxScore)
    }
  } 

  render() {
    const { category, stages, stage, jigsaws, score } = this.state
    const { length } = stages
    return (
      <Row type="flex" justify="center" css={quizStyle}>
        <Col span={24} className="title">Jigsaw Quiz</Col>
        {stage >= length ? (
          <Row className="end-info">
            <Col span={24} className="end finished">Finished!</Col>
            <Col span={24} className="end description">Your total score is...</Col>
            <Col span={24} className="end total-score">{score}</Col>
          </Row>
        ):(
          <>
            <Row className="info">
              <Col span={24} className="stage">{`Stage: ${stage+1}`}</Col>
              <Col span={24} className="score">{`Score: ${score}`}</Col>
            </Row>
            <Col span={24} className="board">
              <Board
                jigsaws={jigsaws}
                onClick={this.onClickJigsaw}
                category={category}
                image={stages[stage].image}
              />
            </Col>
            <Col span={24} className="answer">
              <Input
                placeholder="Enter your answer and press enter."
                onPressEnter={this.onEnterAnswer}
              />
            </Col>
          </>
        )}
        </Row>
    )
  }
}

export default Quiz
